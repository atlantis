/*
 * interface.js - The Atlantis user interface
 * (c) 2008  Sander Dijkhuis <sander.dijkhuis@gmail.com>
 * Still need to decide about licensing.  Ask if interested.
 */

var Atlantis = {
    DEBUG: true,

    CTRL_KEY_CODE: 17,
    BACKSP_KEY_CODE: 8,
    DOC_KEY_CODE: 19,
    contentElement: null,
    commanding: false,
    commandElts: {              // very bad probably
	block: null,
	string: null,
	alternatives: null
    },
    commandString: '',
    commandMatches: null,
    message: '',
    messageElt: null,
    messageShown: false,
    commands: new Array(),
    initFunctions: new Array(),
    initialized: false,

    init: function() {
	this.contentElement = document.getElementById('content');

	this.commandElts.block  = document.getElementById('command-block');
	this.commandElts.string = document.getElementById('command-string');
	this.commandElts.alternatives =
	    document.getElementById('command-alternatives');

	this.messageElt = document.getElementById('message');

        this.addDefaultCommands();

	this.toggleCommandMode(false);
	this.hideMessage();

	this.contentElement.focus();

        for (i in this.initFunctions)
            this.initFunctions[i]();

        this.initialized = true;
    },

    addInitFunction: function(f) {
        if (this.initalized)
            f();
        else
            this.initFunctions.push(f);
    },

    toggleCommandMode: function(on) {
	this.commanding = on;
	this.commandElts.block.style.visibility = on? 'visible' : 'hidden';
	if (!on) {
	    this.setCommandAlternatives('');
	    this.setCommandString('');
	}
    },

    addCommandChar: function(c) {
	this.setCommandString(this.commandString + c);
        this.updateCommandMatches();
    },

    updateCommandMatches: function() {
        this.commandMatches = new Array();
        var s = '';
        var match;
	for (i in this.commands) {
	    match = this.matchCommand(this.commands[i].name,
                                      this.commandString);
	    if (match != null) {
		s += '<li>' + match;
		this.commandMatches.push(this.commands[i].name);
	    }
	}

	this.setCommandAlternatives(s);
    },

    matchCommand: function(command, input) {
	var i = 0;
	var html = '';
	var j;
	var c;
	var foundIt = false;
	for (j in command) {
	    c = command[j];
	    if (input[i] == c && !foundIt) {
		i++;
		if (i == input.length)
		    foundIt = true;
		html += '<b>' + c + '</b>';
	    } else {
		html += c;
	    }
	}
	if (foundIt) {
	    return html;
	}
	else
	    return null;
    },

    deleteCommandChar: function() {
	this.setCommandString(this.commandString.substring(0,
							   this.commandString.
							   length - 1));
        this.updateCommandMatches();
    },

    setCommandString: function(s) {
	this.commandString = this.commandElts.string.innerHTML = s;
    },

    setCommandAlternatives: function(s) {
	this.commandElts.alternatives.style.visibility =
            s? 'visible' : 'hidden';
	this.commandElts.alternatives.innerHTML = s;
    },

    execute: function(command) {
        var available;
        for (i in this.commands)
            if (command == this.commands[i].name)
                return this.commands[i].run();
    },

    showUnknownCommand: function(command) {
	this.showMessage('Unknown command: <b>' + command + '</b>');
    },

    showMessage: function(s) {
	this.messageShown = true;
	this.messageElt.style.visibility = 'visible';
	this.messageElt.childNodes[0].innerHTML = s;
    },

    hideMessage: function() {
	this.messageShown = false;
	this.messageElt.style.visibility = 'hidden';
    },

    insertHTML: function(s) {
	document.execCommand('insertHTML', undefined, s);
    },

    addCommand: function(command) {
        this.commands.push(command);
        this.commands.sort();
    },

    getXMLHttpRequestObject: function() {
        var obj = null;

        if (window.XMLHttpRequest)
            obj = new XMLHttpRequest();
        else if (window.ActiveXObject)
            obj = new ActiveXObject('Microsoft.XMLHTTP');

        return obj;
    },

    loadService: function(serviceId) {
        this.loadScript('/-/' + serviceId + '/main.js');
    },

    loadScript: function(url) {
        var xhr = this.getXMLHttpRequestObject();
        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4)
                return;
            if (xhr.status == 200)
                (function() { eval(xhr.responseText); })();
        };
        xhr.open('GET', url, true);
        xhr.send(null);
    }
};

/* temporary function for adding temporary commands quickly and uglily */
Atlantis.addDefaultCommands = function() {
    var i;
    var cmd;
    var cmds =
    [
     ['undo', '', function() {
             document.execCommand('undo', undefined, undefined);
         }, null],
     ['redo', '', function() {
             document.execCommand('redo', undefined, undefined);
         }, null],
     ['new document', '', function() {
             // doesn't work if in <ul>
             Atlantis.insertHTML('<hr class=doc-char>');
         }, null],
     ['commands', '', function() {
             var i;
             var command;
             s = '<h3>Commands</h3><ul>';
             for (i in Atlantis.commands) {
                 command = Atlantis.commands[i];
                 s += '<li><b>' + command.name.replace(/_/g, ' ') + '</b>';
                 if (command.doc)
                     s += ' - ' + command.doc;
             }
             s += '</ul>';
             Atlantis.insertHTML(s);
         }, null],
     ['hello world', '', function() {
             Atlantis.insertHTML('Hello, World!');
         }, null]
     ];

    for (i in cmds) {
        cmd = new Atlantis.Command();
        cmd.name = cmds[i][0];
        cmd.doc = cmds[i][1];
        cmd.run = cmds[i][2];
        cmd.data = cmds[i][3];
        Atlantis.addCommand(cmd);
    }

    cmd = new Atlantis.Command();
    cmd.name = 'data test';
    cmd.doc = 'Testing data.';
    cmd.data = { i: 0 };
    cmd.run = function() {
        Atlantis.insertHTML(cmd.data.i++);
    };
    Atlantis.addCommand(cmd);
};

Atlantis.Command = function() {
};

Atlantis.Command.prototype = {
    name: '',
    doc: '',
    run: null,
    data: null,
    toString: function() {
        return this.name;
    },
};

function onkeydown(evt) {
    if (Atlantis.messageShown)
	Atlantis.hideMessage();

    if (evt.keyCode == Atlantis.CTRL_KEY_CODE) {
	Atlantis.toggleCommandMode(true);
	return false;
    }
}

function onkeyup(evt) {
    if (evt.keyCode == Atlantis.CTRL_KEY_CODE) {
	if (Atlantis.commandString) {
	    if (Atlantis.commandMatches.length > 0)
		Atlantis.execute(Atlantis.commandMatches[0]);
	    else
		Atlantis.showUnknownCommand(Atlantis.commandString);
	}
	Atlantis.toggleCommandMode(false);
    }
}

function onkeypress(evt) {
    if (Atlantis.commanding) {
	if (evt.which == Atlantis.BACKSP_KEY_CODE)
	    Atlantis.deleteCommandChar();
	else
	    Atlantis.addCommandChar(String.fromCharCode(evt.charCode));
	return false;
    } else if (evt.keyCode == Atlantis.DOC_KEY_CODE)
	Atlantis.execute('new document');
}

function oncontextmenu(evt) {
    return false;
}

function onload(evt) {
    Atlantis.init();
}

/*
 * atlantis-server.h - The Atlantis server
 * (c) 2008  Sander Dijkhuis <sander.dijkhuis@gmail.com>
 * Still need to decide about licensing.  Ask if interested.
 */

#ifndef ATLANTIS_SERVER_H
#define ATLANTIS_SERVER_H

G_BEGIN_DECLS

#include <glib-object.h>

#define ATLANTIS_TYPE_SERVER (atlantis_server_get_type ())
#define ATLANTIS_SERVER(obj) (G_TYPE_CHECK_CLASS_CAST			\
			      ((obj), ATLANTIS_TYPE_SERVER, AtlantisServer))
#define ATLANTIS_SERVER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST		\
				      ((klass), ATLANTIS_TYPE_SERVER,	\
				       AtlantisServerClass))
#define ATLANTIS_IS_SERVER(obj) (G_TYPE_CHECK_INSTANCE_TYPE	\
				 ((obj), ATLANTIS_TYPE_SERVER))
#define ATLANTIS_SERVER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS	\
					((obj), ATLANTIS_TYPE_SERVER,	\
					 AtlantisServerClass))

typedef struct _AtlantisServer AtlantisServer;
typedef struct _AtlantisServerClass AtlantisServerClass;
typedef struct _AtlantisServerPrivate AtlantisServerPrivate;

struct _AtlantisServer
{
  GObject parent;

  /*< private >*/
  AtlantisServerPrivate *private;
};

struct _AtlantisServerClass
{
  GObjectClass parent;
  /* class members go here */
};

GType
atlantis_server_get_type (void);

AtlantisServer *
atlantis_server_new (void);

gboolean
atlantis_server_register_service (AtlantisServer *server,
                                  gchar *bus_name,
                                  gchar *object_path,
                                  gchar *service_id,
                                  GError **error);

G_END_DECLS

#endif

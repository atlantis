var i;
var cmd;
var cmds = [
    ['bold', 'Toggles the bold font.', function() {
        document.execCommand('bold', undefined, undefined);
     }, null],
    ['italic', 'Toggles the italic font.', function() {
        document.execCommand('italic', undefined, undefined);
     }, null],
    ['unordered list', '', function() {
        document.execCommand('insertUnorderedList', undefined, undefined);
     }, null],
    ['ordered list', '', function() {
        document.execCommand('insertOrderedList', undefined, undefined);
     }, null],
    ['paragraph', '', function() {
        document.execCommand('insertParagraph', undefined, undefined);
     }, null],
    ['heading', '', function() {
        document.execCommand('formatBlock', undefined, 'h1');
     }, null],
];

for (i in cmds) {
    cmd = new Atlantis.Command();
    cmd.name = cmds[i][0];
    cmd.doc = cmds[i][1];
    cmd.run = cmds[i][2];
    cmd.data = cmds[i][3];
    Atlantis.addCommand(cmd);
}

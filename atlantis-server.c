/*
 * atlantis-server.c - The Atlantis server
 * (c) 2008  Sander Dijkhuis <sander.dijkhuis@gmail.com>
 * Still need to decide about licensing.  Ask if interested.
 */

#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <netdb.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "atlantis-server.h"
#include "atlantis-server-glue.h"

#define ATLANTIS_SERVER_PORT 8000

struct _AtlantisServerPrivate
{
  DBusGConnection *dbus_connection;
  GData **services;
};

typedef struct _AtlantisService AtlantisService;

struct _AtlantisService
{
  gchar *bus_name;
  gchar *object_path;
};

/* FIXME: Why should we do this? */
extern void
close (int);

extern DBusConnection *
dbus_g_connection_get_connection (DBusGConnection *gconnection);
/* end of FIXME */

void
web_respond (int cfd, gchar *resp)
{
  send (cfd, resp, strlen (resp), 0);
}

gchar *
get_response_from_service (AtlantisServer *server,
                           gchar *url)
{
  GRegex *regex = g_regex_new ("^/-/(?P<service>[^/]+)(?P<path>/.*)$",
                               0, 0, NULL);
  GMatchInfo *match_info;
  gchar *service_id;
  GError *error;
  DBusGProxy *proxy;
  AtlantisService *service;
  gchar *content;
  gchar *path;

  if (!g_regex_match (regex, url, 0, &match_info))
    {
      g_regex_unref (regex);
      g_match_info_free (match_info);
      return NULL;
    }

  service_id = g_match_info_fetch_named (match_info, "service");
  path = g_match_info_fetch_named (match_info, "path");

  g_regex_unref (regex);
  g_match_info_free (match_info);

  service = g_datalist_get_data (server->private->services, service_id);
  if (service == NULL)
    return NULL;

  proxy = dbus_g_proxy_new_for_name (server->private->dbus_connection,
                                     service->bus_name,
                                     service->object_path,
                                     "nl.momple.Atlantis.Service");

  error = NULL;
  if (!dbus_g_proxy_call (proxy, "GetContent", &error,
                          G_TYPE_STRING, service_id,
                          G_TYPE_STRING, path,
                          G_TYPE_INVALID,
                          G_TYPE_STRING, &content,
                          G_TYPE_INVALID))
    {
      g_message ("D-Bus error: %s", error->message);
      return NULL;
    }

  g_free (service_id);

  return content;
}

void
add_service_init (GQuark key_id, gpointer data, gpointer user_data)
{
  int *cfd = (int *)user_data;
  gchar *service_id = (gchar *)(g_quark_to_string (key_id));
  web_respond (*cfd, g_strconcat ("<script src='-/", service_id,
                                  "/main.js'></script>\n", NULL));
}

void
add_service_script (GQuark key_id, gpointer data, gpointer user_data)
{
  struct DataStruct
  {
    int cfd;
    AtlantisServer *server;
  };
  struct DataStruct *ds = (struct DataStruct *)user_data;
  gchar *service_id = (gchar *)(g_quark_to_string (key_id));
  web_respond (ds->cfd, g_strconcat ("    loadService('", service_id, "');\n",
                                     NULL));
}

void
send_web_response (AtlantisServer *self, char *url, int cfd)
{
  GIOChannel *file;
  gchar *content;
  gsize content_length;
  GError **error = NULL;

  if (!strcmp (url, "/"))
    {
      web_respond (cfd,
                   "HTTP/1.1 200 OK\n"
                   "Content-Type: text/html; charset=UTF-8\n"
                   "\n"
                   "<!doctype html>\n"
                   "<title>Atlantis</title>\n"
                   "<script src=interface.js></script>\n"
                   "<script src=services.js></script>\n"
                   "<link rel=stylesheet href=interface.css>\n"
                   "<div id=command-block>\n"
                   " <p id=command-string></p><br>\n"
                   " <ul id=command-alternatives></ul>\n"
                   "</div>\n"
                   "<div id=message><p></div>\n"
                   "<div id=content contentEditable>\n"
                   "<h1>Test</h1>\n"
                   "<p>Hello, world!\n"
                   "<hr class=doc-char>\n"
                   "<p>Bye, world!\n"
                   "</div>"
                   );
    }
  else if (!strcmp (url, "/interface.js"))
    {
      web_respond (cfd,
                   "HTTP/1.1 200 OK\n"
                   "Content-Type: text/javascript\n\n");

      file = g_io_channel_new_file ("interface.js", "r", error);
      g_io_channel_read_to_end (file, &content, &content_length, error);

      web_respond (cfd, content);

      g_free (content);
      g_io_channel_shutdown (file, FALSE, error);
    }
  else if (!strcmp (url, "/interface.css"))
    {
      web_respond (cfd,
                   "HTTP/1.1 200 OK\n"
                   "Content-Type: text/css\n\n");

      file = g_io_channel_new_file ("interface.css", "r", error);
      g_io_channel_read_to_end (file, &content, &content_length, error);

      web_respond (cfd, content);

      g_free (content);
      g_io_channel_shutdown (file, FALSE, error);
    }
  else if (!strcmp (url, "/services.js"))
    {
      struct
      {
        int cfd;
        AtlantisServer *server;
      } data;
      data.cfd = cfd;
      data.server = self;

      web_respond (cfd,
                   "HTTP/1.1 200 OK\n"
                   "Content-Type: text/javascript\n\n");

      web_respond (cfd, "with (Atlantis) {\n"
                        "  addInitFunction(function() {\n");
      g_datalist_foreach (self->private->services, add_service_script, &data);
      web_respond (cfd, "  });\n}");
    }
  else if ((content = get_response_from_service (self, url)) != NULL)
    {
      web_respond (cfd, content);
    }
  else
    {
      web_respond (cfd, "HTTP/1.1 404 Not Found\n\n404, not found.");
    }
}

gboolean
incoming_cb (GIOChannel *source, GIOCondition condition, gpointer data)
{
  int cfd;
  struct sockaddr_in sockaddr;
  unsigned int socklen = sizeof (sockaddr);
  char buf[256];
  char url[256];
  int i, j;
  int fd = g_io_channel_unix_get_fd (source);
  if (condition != G_IO_IN)
    return TRUE;

  if ((cfd = accept (fd, (struct sockaddr *)&sockaddr, &socklen)) < 0)
    return TRUE;

  recv (cfd, buf, sizeof (buf), 0);

  /* Put the requested URL in url. */
  for (i = 0; i < 256 && buf[i] != ' '; i++);
  for (i++, j = 0; i < 256 && buf[i] != ' '; i++, j++)
    url[j] = buf[i];
  url[j] = '\0';

  send_web_response ((AtlantisServer *)data, url, cfd);

  close (cfd);

  return TRUE;
}

void
atlantis_server_class_init (gpointer g_class,
			   gpointer g_class_data)
{
  dbus_g_object_type_install_info (ATLANTIS_TYPE_SERVER,
				   &dbus_glib_atlantis_server_object_info);
}

void
atlantis_server_instance_init (GTypeInstance   *instance,
			       gpointer        g_class)
{
  AtlantisServer *self = (AtlantisServer *)instance;
  GError *error = NULL;
  DBusError *dbus_error = NULL;
  struct sockaddr_in sockaddr;
  int fd;

  self->private = g_new0 (AtlantisServerPrivate, 1);

  self->private->services = g_malloc (sizeof (GData *));
  *(self->private->services) = NULL;
  g_datalist_init (self->private->services);

  if ((self->private->dbus_connection =
       dbus_g_bus_get (DBUS_BUS_SESSION, &error)) == NULL)
    {
      g_error ("Failed to open D-Bus connection: %s\n", error->message);
    }

  dbus_bus_request_name (dbus_g_connection_get_connection
			 (self->private->dbus_connection),
			 "nl.momple.Atlantis.Server", 0, dbus_error);

  dbus_g_connection_register_g_object (self->private->dbus_connection,
				       "/nl/momple/Atlantis/Server",
				       G_OBJECT (self));

  if ((fd = socket (PF_INET, SOCK_STREAM, 0)) < 0)
    {
      g_error ("Couldn't open the server socket.");
    }

  sockaddr.sin_family = AF_INET;
  sockaddr.sin_addr.s_addr = INADDR_ANY;
  sockaddr.sin_port = htons (ATLANTIS_SERVER_PORT);

  if ((bind (fd, (struct sockaddr *)&sockaddr, sizeof (sockaddr))) < 0)
    {
      g_error ("Couldn't bind to the server socket.");
    }

  listen (fd, 1);
  g_io_add_watch (g_io_channel_unix_new (fd), G_IO_IN, incoming_cb, self);

  g_message ("Started the server at http://localhost:%d/",
	     ATLANTIS_SERVER_PORT);
}

AtlantisServer *
atlantis_server_new (void)
{
  return g_object_new (ATLANTIS_TYPE_SERVER, NULL);
}

gboolean
atlantis_server_register_service (AtlantisServer *self,
                                  gchar *bus_name,
                                  gchar *object_path,
                                  gchar *service_id,
                                  GError **error)
{
  AtlantisService *service = g_malloc (sizeof (AtlantisService));

  service->bus_name = g_strdup (bus_name);
  service->object_path = g_strdup (object_path);
  g_datalist_set_data (self->private->services, service_id, service);

  g_message ("Service %s registered", service_id);

  return TRUE;
}

GType
atlantis_server_get_type (void)
{
  static GType type = 0;
  if (!type)
    {
      static const GTypeInfo info =
	{
	  sizeof (AtlantisServerClass),
	  NULL,   /* base_init */
	  NULL,   /* base_finalize */
	  &atlantis_server_class_init,
	  NULL,   /* class_finalize */
	  NULL,   /* class_data */
	  sizeof (AtlantisServer),
	  0,      /* n_preallocs */
	  &atlantis_server_instance_init
	};

      type = g_type_register_static (G_TYPE_OBJECT,
				     "AtlantisServerType",
				     &info, 0);
    }
  return type;
}

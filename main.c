/*
 * main.c - Runs the Atlantis server
 * (c) 2008  Sander Dijkhuis <sander.dijkhuis@gmail.com>
 * Still need to decide about licensing.  Ask if interested.
 */

#include <dbus/dbus-glib.h>
#include <glib.h>
#include "atlantis-server.h"

int
main (int argc, char *argv[])
{
  AtlantisServer *server;
  GMainLoop *loop = g_main_loop_new (NULL, TRUE);

  g_type_init ();

  server = atlantis_server_new ();

  g_main_loop_run (loop);
}

PROGNAME=atlantis-server

CC=cc
CFLAGS=-g -Wall -pedantic -ansi `pkg-config --cflags glib-2.0 dbus-glib-1` -DG_LOG_DOMAIN=\"Atlantis\"
LFLAGS=-lm `pkg-config --libs glib-2.0 dbus-glib-1`
RM=rm -rf
OFILES=main.o atlantis-server.o

$(PROGNAME): $(OFILES)
	$(CC) $(CFLAGS) $(OFILES) $(LFLAGS) -o $(PROGNAME)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) *.o $(PROGNAME)

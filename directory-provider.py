# directory-provider.py - Registers and runs single-JavaScript-file services
# (c) 2008  Sander Dijkhuis <sander.dijkhuis@gmail.com>

from __future__ import with_statement
import dbus
import dbus.mainloop.glib
import dbus.service
import gobject
import os
import sys

USAGE = """Usage: python directory-provider.py /PATH/TO/SERVICE_DIRECTORY
Provides a service X for each file X.js in the specified directory."""

class DirectoryProvider(dbus.service.Object):
    def __init__(self, path):
        self.path = path

        object_path = '/nl/momple/Atlantis/DirectoryProvider'
        dbus.service.Object.__init__(self, dbus.SessionBus(), object_path)

        bus_name = 'nl.momple.Atlantis.DirectoryProvider'
        self.connection.request_name(bus_name)

        server = self.connection.get_object('nl.momple.Atlantis.Server',
                                            '/nl/momple/Atlantis/Server')

        for file in os.listdir(path):
            if file[-3:] == '.js':
                server.RegisterService(bus_name, object_path, file[:-3])

    @dbus.service.method(dbus_interface='nl.momple.Atlantis.Service',
                         in_signature='ss', out_signature='s')
    def GetContent(self, service, url_path):
        if url_path == '/main.js' \
        and os.path.exists('%s/%s.js' % (self.path, service)):
            print '200: /-/%s/main.js' % service
            with open('%s/%s.js' % (self.path, service)) as f:
                return 'HTTP/1.1 200 OK\nContent-Type: text/javascript\n\n' \
                       + ''.join(f.readlines())
        else:
            print '404: %s' % url_path
            return 'HTTP/1.1 404 Not Found\n\nNot found'

if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit(USAGE)

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    ts = DirectoryProvider(sys.argv[1])

    loop = gobject.MainLoop()
    try:
        loop.run()
    except KeyboardInterrupt:
        print 'Goodbye'
        loop.quit()
